import React, { useState } from "react";
import TutorialDataService from "../services/TutorialServices";

const AddTutorial = () => {
  const initialTutorialState = {
    id :null,
    title : " ",
    description : " "
  };

  const[tutorial, setTutorial] = useState(initialTutorialState);
  const [ submitted , setSubmitted] = useState(false);
  const handleInputChange = event =>{
    const {name , value } = event.target;
    setTutorial({...tutorial,[name] : value });
  };
  const saveTutorial = () => {
    var data = {
      title:tutorial.title,
      description : tutorial.description
    };

    TutorialDataService.create(data)
    .then(response => {
      setTutorial({
        id :response.data.id,
        title : response.data.title,
        description : response.data.description,
      });

      setSubmitted(true);
      console.log(response.data);
    })

    .catch(e => {
      console.log(e);
    });
  };
  const newTutorial = () => {
    setTutorial(initialTutorialState);
    setSubmitted(false);
  };
  return (
    <div>
      {submitted ? (
        <div>
        <h5>You submitted successfully</h5>
        <button onClick={newTutorial}></button>
        </div>
      ) : (
        <div>
          <div>
            <label htmlFor="title">Title</label>
            <input
            type= "title"
            className = "form"
            id = "title"
            required
            value = {tutorial.title}
            onChange = {handleInputChange}
            name = "title"/>
            </div>
            <div>
              <label htmlFor="description">Description</label>
              <input
              type= "text"
              className="form"
              id="description"
              required
              value={tutorial.description}
              onChange = {handleInputChange}
              name = "description"
              />
            </div>
            <button onClick={saveTutorial} className = "btn ">Submit</button>
        </div>

      )}
    </div>
  );
};
export default AddTutorial;









